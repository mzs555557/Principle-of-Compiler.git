import LL1.*;

import java.util.Map;
import java.util.Set;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // 获取初始化的G
        GetG getG = new GetG();
        G g = getG.outputG();

        // 对G进行左递归
        ClearRecursion clearRecursion = new ClearRecursion(g);
        G clearG = clearRecursion.clearLeftRecursion();

        // 获取 first 集
        First first = new First(clearG);
        Map<String, Set<String>> firstEpisode = first.getFirstEpisode();

        // 获取 follow 集
        Follow follow = new Follow(firstEpisode,clearG);
        Map<String,Set<String>> followEpisode = follow.getFollowEpisode();

        // 获取 select 集
        Select select = new Select(firstEpisode,followEpisode,clearG);
        Map<String,Set<String>> selectEpisode = select.getSelectEpisode();
        // 输入需要匹配的符号串
        String inputString = "i+i*i";

        // 开始LL1文法的分析
        LL1 ll1 = new LL1(selectEpisode,clearG, inputString);
        ll1.startLL1();

    }

}
