package LL1;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class First {
    public Map<String, Set<String>> first = new TreeMap<>(); //所求的first集
    public G g;


    public First(G g) {
        this.g = g;
    }

    // 求First集的方法
    public Map<String,Set<String>> getFirstEpisode(){
        for (int i =0; i< g.getpCount(); i++ ) {
            P p = g.getpList().get(i);
            getFirst(p); // 获得每个产生式的First集
        }

        for (Map.Entry<String,Set<String>> entry : first.entrySet()) {
            System.out.println("First(" + entry.getKey()+")=" +entry.getValue()+"");
        }

        return first;
    }

    // 求产生式的First集
    private Set<String> getFirst(P p) {
        if (first.containsKey(p.getKey())) {
            return first.get(p.getKey());
        }

        Set<String> set = new TreeSet<>();
        // 循环产生式
        for (int i=0; i< p.getValue().length;i++) {
            Set<String> set2 = new TreeSet<>();
            for (int j= 0; j< p.getValue()[i].length(); j++) {
                String node = p.getValue()[i].charAt(j) + "";

                if (isVNorVT(node, g.getVT())){ // 如果是终结符

                    set2.add(node);
                    break;
                } else if (isVNorVT(node, g.getVN())){ // 如果是非终结符
                    if (j+1<p.getValue()[i].length()&&p.getValue()[i].charAt(j+1)=='\'') { // 防止出现 "E'"之类的情况
                        node += p.getValue()[i].charAt(j+1);
                        ++j;
                    }

                    P result = g.findPbyKey(node);
                    if (result!=null) { // 能够找到
                        Set<String> newSet = getFirst(result);

                        if (newSet.contains("ε")&&j==p.getValue()[i].length()-1){ //  S->ABC,只有当 A->ε B->ε C->ε, 才可以说 ε属于first(S)
                            set2.addAll(newSet);
                        }
                        if (newSet.contains("ε")&&j< p.getValue()[i].length() - 1) {
                            newSet.remove("ε");
                            set2.addAll(newSet);
                        }

                        if (!newSet.contains("ε")){ // 如果不包含 ε , 结束
                            set2.addAll(newSet);
                            break;
                        }

                    }else {
                        try {
                            throw new Exception("所获取的p为null");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else if (node.equals("ε")&&node.length()==p.getValue()[i].length()){ // 如果可以直接推导出 S-> ε
                    set2.add("ε"); // 之后还需要循环去找 下一个 符号
                }


            }
            set.addAll(set2);
        }
        first.put(p.getKey(), set);
        return set;
    }
    // 判断是终结符还是非终结符
    private boolean isVNorVT(String charac , String[] value) {
        boolean bool = false;

        for (String val:value) {
            if (charac.equals(val)) {
                bool = true;
            }
        }
        return bool;
    }

}
