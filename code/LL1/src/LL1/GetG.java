package LL1;

public class GetG {
    public G outputG() {

        return initG();
    }

    private G initG() {
        G g = new G();

//        // 非LL1文法
//        g.setVN("SQR"); // 设置 非终结符
//        g.setVT("abc"); // 设置终结符
////        g.setpCount(3); // 设置产生式的数量
//        g.setStart("S"); // 设置开始符号
//
////        for (int i =0;i< g.getVN().length;i++) {
////            System.out.println(g.getVN()[i]);
////        }
//
//        P p1 = new P();
//
//        // E->E+T|T
//        p1.setKey("S");
//        p1.setValue("Qc|c");
////        System.out.println(p1.getCount());
////        for (int i =0; i< p1.getCount();i++) {
////            System.out.println(p1.getValue()[i]);
////        }
//        g.addP(p1);
//
//        P p2 = new P();
//        // T->T*F|F
//        p2.setKey("Q");
//        p2.setValue("Rb|b");
//        g.addP(p2);
//
//
//        P p3 =new P();
//        // F->i|(E)
//        p3.setKey("R");
//        p3.setValue("Sa|a");
//        g.addP(p3);

        g.setVN("ETF");
        g.setVT("+*i()");

        P p1 = new P();
        // E->E+T|T
        p1.setKey("E");
        p1.setValue("E+T|T");

        P p2 = new P();
        // T -> T*F|F
        p2.setKey("T");
        p2.setValue("T*F|F");

        P p3 = new P();
        // F->i|(E)
        p3.setKey("F");
        p3.setValue("i|(E)");
        g.addP(p1);
        g.addP(p2);
        g.addP(p3);

        return g;
    }
}
