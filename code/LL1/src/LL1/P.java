package LL1;

import java.io.Serializable;
import java.util.Set;

public class P implements Serializable { // 表示一条产生式
    String key; // 产生式的左部
    String[] value=new String[]{}; // 产生式的右部

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String[] getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value.split("\\|");
    }
    public void addValue(String value) {
        this.value = insert(value, getValue());
    }

    public void addValue(String[] values) {
        for (String value : values) {
            addValue(value);
        }
    }

    public void deleteValue(String value) {
        this.value = delete(value, getValue());
    }

    public int getCount() {
        return this.value.length;
    }

    private String[] delete(String value, String[] values) {
        int length = values.length;
        String[] newValues = new String[length-1];
        int index = 0;
        for (int i = 0; i< length; i++) {
            if (values[i].equals(value)) {
                index = i;
                break;
            }
        }

        System.arraycopy(values,0,newValues,0,index);
        if (index < length -1) {
            System.arraycopy(values, index+1,newValues,index,newValues.length-index);
        }

        return newValues;
    }

    private String[] insert(String value, String[] values) {
        int length = values.length;
        String[] newValues = new String[length + 1];
        newValues[length] = value;
        System.arraycopy(values, 0, newValues, 0, length);
        return newValues;
    }
    // 设置 右部规则的数量
//        public void setCount(int count) {
//            if (count == 0) { // 如果没有设置, 根据value自动算出有几组规则
//                this.count = value.split("\\|").length;
//            } else { // 设置的话就赋值
//                this.count = count;
//            }
//        }
}