package LL1;

public class ClearRecursion {

    private G g;

    public ClearRecursion(G g) {
        this.g = g;
    }

    public G clearLeftRecursion() {
        transformP(g); // 将间接左递归转化为直接左递归

        clearRecursion(g); // 消除直接左递归

        return g;
    }
    // 将间接左递归转化为直接左递归
    private void transformP(G g) {
        for (int i = 0; i<g.getpCount();i++) {
            P p = g.getpList().get(i);
            for (int j =0; j<i;j++) {
                replaceP(p, g.getpList().get(j));
            }
        }
    }

    // 可能代替g.getpList.get(i)的产生式
    private void replaceP(P p1, P p2) {
        // 获取p2的左部非终结符
        String key = p2.getKey();
        // 获取p1的右步文法规则
        String[] values1 = p1.getValue();
        // 获取p2的右部文法规则
        String[] values2= p2.getValue();

        for (String s : values1) {
            if (s.substring(0, 1).equals(key)) { // 进行对比,如果相同
                String value1 = s.substring(1);
                String[] newValues1 = new String[values2.length];
                for (int j = 0; j < values2.length; j++) {
                    newValues1[j] = values2[j] + value1;
                }
                p1.deleteValue(s);

                p1.addValue(newValues1);
            }
        }
    }

    // 消除直接左递归
    private void clearRecursion(G g) {
        for (int i=0; i< g.getpCount(); i++) {
            P p = g.getpList().get(i);

            String key = p.getKey(); // 获取左部非终结符
            String [] values = p.getValue(); // 获取规则

            boolean has = false; // 是否符合直接左递归

            for (String value : values) {
                String character = value.substring(0, 1); // 获取第一个符号
                if (character.equals(key)) { // 与左部相同
                    putInP(key, value , g);
                    p.deleteValue(value);
                    has = true;
                }
            }
            if (has&&p.getValue().length>0) {
                for (int k =0; k<p.getValue().length;k++) {
                    p.value[k]+=key+"'";
                }
            }

        }
    }

    /**
     * 将一条文法规则放入新的产生式中 , 即
     * E->E+T|T
     * 转化为
     * E->TE'
     * E'->+TE'|ε // putInP()
     */
    private void putInP(String key, String value,G g) {
        key = key+"'"; // 新符号
        value = value.substring(1) + key;
        boolean bool = true;


        for (int i =0; i<g.getpList().size(); i++) {
            if (g.getpList().get(i).getKey().equals(key)) { // 判断产生式中是否有此新符号,有则直接添加
                g.getpList().get(i).addValue(value);
                bool = false;
            }
        }
        if (bool) { // 没有的话,新建
            P p = new P();
            p.setKey(key);
            p.addValue(value);
            p.addValue("ε");
            g.addP(p);
        }
    }

    public void showClearResult() {
        G g = clearLeftRecursion();

        // 将结果打印
        for (int m =0; m<g.getpCount(); m++) {
            P p = g.getpList().get(m);
            StringBuilder value = new StringBuilder();
            for (int n =0; n<p.getValue().length; n++) {
                value.append(p.getValue()[n]).append("|");
            }
            System.out.println(p.getKey()+"->"+ value.substring(0,value.length()-1));

        }
    }

}
