package LL1;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Select {
    private Map<String, Set<String>> select = new TreeMap<>();

    private Map<String, Set<String>> first;
    private Map<String,Set<String>> follow;
    private G g;

    public Select(Map<String, Set<String>> first, Map<String, Set<String>> follow, G g) {
        this.first = first;
        this.follow = follow;
        this.g = g;
    }

    // 获取并打印select集
    public Map<String, Set<String>> getSelectEpisode(){
        for (int i =0; i<g.getpCount();i++) {
            getSelect(g.getpList().get(i));
        }

        for (Map.Entry<String,Set<String>> entry : select.entrySet()) {
            System.out.println("Select(" + entry.getKey()+")=" +entry.getValue()+"");
        }

        return select;
    }

    private void getSelect(P p) {

        for (int i= 0; i<p.getValue().length;i++) {

            String key = p.getKey()+"->"+p.getValue()[i]; // 定义 Select(A->BC) 的key值
            Set<String> selectSet = new TreeSet<>(); // 放置到这里

            for (int j = 0; j<p.getValue()[i].length(); j++) {
                String node = p.getValue()[i].charAt(j) + "";
                if (j+1<p.getValue()[i].length()&&p.getValue()[i].charAt(j+1)=='\''){
                    node += p.getValue()[i].charAt(j+1);
                    ++j;
                }

                if (isVNorVT(node,g.getVT())) { // 如果是终结符
                    selectSet.add(node);
                    break;
                }
                if (node.equals("ε")) {
                    selectSet.addAll(follow.get(p.getKey()));
                    break;
                }
                if (first.containsKey(node)) { // 表明是非终结符
                    selectSet.addAll(first.get(node));
                    if (!first.get(node).contains("ε")) {
//                        selectSet.addAll(first.get(node));
                        break;
                    }
                    if (first.get(node).contains("ε")&&j==p.getValue()[i].length()-1) {
//                        selectSet.addAll(first.get(node));
                        selectSet.addAll(follow.get(p.getKey()));
                        break;
                    }
//                    selectSet.addAll(first.get(node));

                }

            }
            selectSet.remove("ε");
            select.put(key, selectSet);
        }
    }

    // 判断是终结符还是非终结符
    private boolean isVNorVT(String charac , String[] value) {
        boolean bool = false;

        for (String val:value) {
            if (charac.equals(val)) {
                bool = true;
            }
        }
        return bool;
    }

}
