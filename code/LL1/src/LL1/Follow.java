package LL1;

import java.util.*;

public class Follow {
    private Map<String, Set<String>> follow = new TreeMap<>();

    private Map<String,Set<String>> first;
    private G g;

    public Follow(Map<String, Set<String>> first, G g) {
        this.first = first;
        this.g = g;
    }

    // 获取并打印 follow集
    public Map<String,Set<String>> getFollowEpisode() {
        for (int i = 0; i<g.getpCount();i++) {
            // 获取
            String key = g.getpList().get(i).getKey();

            if (i==0) {
                follow.put(key , Collections.singleton("#"));
            }
            getFollow(key);
        }
        for (Map.Entry<String,Set<String>> entry : follow.entrySet()) {
            System.out.println("Follow(" + entry.getKey()+")=" +entry.getValue()+"");
        }
        return follow;
    }

    // 获取单个产生式的 follow集
    public void getFollow(String key) {
        Set<String> followSet = new TreeSet<>();

        // 遍历所有的产生式
        for (int i =0; i<g.getpCount(); i++) {
            P p = g.getpList().get(i);
            for (int j = 0; j<p.getValue().length; j++) {
                int index = p.getValue()[j].indexOf(key); // 找到此非终结符对应的下标

                while (index != -1) { // 如果有此非终结符
                    int nextIndex = index + 1; // 便于循环

                    if (key.length()==1&&index+1 < p.getValue()[j].length() && p.getValue()[j].charAt(index+1)== '\'') { // 如果是 E' 之类的形式
                        index = p.getValue()[j].indexOf(key, index+1); //// 恰巧 key 是 E 的情况, 需要跳过此处 从 E' 之后 重新检索
                        continue;
                    }

                    //  开始计算 key 之后的 情况
                    index = index + key.length();

                    if (index == p.getValue()[j].length()) { // 如果 结果为 A-> ..E , 即 E 在 产生式的最后
                        if (follow.get(p.getKey()) == null) { // 如果 不能获取到 目前A的follow集, 需要进行求取
                            if (p.getKey().equals(key)&&key.length()==2) { // R' -> ...R'的情况, 如果是 R->..R 呢?
                                String newKey = key.charAt(0)+"";
                                if (follow.get(newKey)== null) {
                                    getFollow(newKey);
                                }
                                if (follow.get(key)==null) {
                                    follow.put(key, follow.get(newKey));
                                } else {
                                    follow.get(key).addAll(follow.get(newKey));
                                }
                            }
                            getFollow(p.getKey());
                        }

                        followSet.addAll(follow.get(p.getKey()));  // 之后加入

                        if (follow.get(key)== null) { // 如果follow 集中没有此数据
                            follow.put(key,followSet);
                        } else {
                            follow.get(key).addAll(followSet);
                        }

                    } else { // 如果后面跟的还有 A-> ..E..
                        String node = p.getValue()[j].charAt(index) + ""; // 获得下一个 node

                        if (index+1 < p.getValue()[j].length()&& p.getValue()[j].charAt(index + 1) == '\''){
                            node += p.getValue()[j].charAt(index + 1);
                            ++index;
                        }
                        if (isVNorVT(node, g.getVT())) { // 如果是终结符
                            followSet.add(node);
                            if (follow.get(key)== null) { // 如果follow 集中没有此数据
                                follow.put(key,followSet);
                            } else {
                                followSet.addAll(follow.get(key));
                                follow.put(key,followSet);
//                                follow.get(key).addAll(followSet);
                            }
                        } else if (first.containsKey(node)) { // 如果是 非终结符
                            if (first.get(node).contains("ε")) { // 如果出现 A-> ..EB 而B的First集中有ε //需要去掉
                                Set<String> tmpSet = new TreeSet<>();
                                getFirstSetForFollow(tmpSet, p.getValue()[j], index,p.getKey());
                                tmpSet.remove("ε");
                                followSet.addAll(tmpSet);
                                if (follow.get(key)== null) { // 如果follow 集中没有此数据
                                    follow.put(key,followSet);
                                } else {
                                    follow.get(key).addAll(followSet);
                                }

                            } else {
                                followSet.addAll(first.get(node));
                            }
                        }
                    }
                    index = p.getValue()[j].indexOf(key, nextIndex);
                }
            }
        }
    }

    /**
     * 为求follow集而获取所有的first
     * @param tmpSet
     * @param value
     * @param index
     */
    private void getFirstSetForFollow(Set<String> tmpSet, String value, int index,String key) {
        if (index >= value.length()) {
            return;
        }
        if (value.charAt(index)=='\''){
            --index;
        }
        String node = value.charAt(index) + "";
        if (index+1 < value.length() && value.charAt(index+1)=='\'') {
            node += value.charAt(index+1);
            ++index;
        }
        if (first.containsKey(node)) {
            tmpSet.addAll(first.get(node));

            if (first.get(node).contains("ε")){
                if (index==value.length()-1){ // 如果直到最后都是ε, 证明key->ε, 需要加入 follow(key)
                    if (follow.get(key)== null) {
                        getFollow(key);
                    }
                    tmpSet.addAll(follow.get(key));
                }
                getFirstSetForFollow(tmpSet, value, index+1,key);
            }
        } else {
            tmpSet.add(node);
        }

    }

    // 判断是终结符还是非终结符
    private boolean isVNorVT(String charac , String[] value) {
        boolean bool = false;

        for (String val:value) {
            if (charac.equals(val)) {
                bool = true;
            }
        }
        return bool;
    }
}
