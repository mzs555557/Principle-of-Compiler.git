package LL1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class G implements Serializable { // 用于表示该文法
    String[] VN; // 表示非终结符的集合
    String[] VT; // 表示终结符的集合
    List<P> pList = new ArrayList<>(); // 产生式的集合
    String start; // 定义开始符号

    public String[] getVN() {
        return VN;
    }

    public void setVN(String VN) {
        this.VN = VN.split("");
    }
    public void addVN(String VN) {
        this.VN = insert(VN,this.VN);
    }

    public String[] getVT() {
        return VT;
    }

    public void setVT(String VT) {
        this.VT = VT.split("");
    }

    public List<P> getpList() {
        return pList;
    }

    public void setpList(List<P> pList) {
        this.pList = pList;
    }

    public void addP(P p) {
        this.pList.add(p);
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public P findPbyKey(String key) {
        P result = null;
        for (P p: pList) {
            if (p.getKey().equals(key)) {
                result = p;
            }
        }
        return result;
    }

    public int getpCount() {
        return pList.size();
    }

    // 向数组中插入元素,插入到最后一个
    private String[] insert(String VN, String[] VNarray) {
        int length = VNarray.length;
        String[] newVNarray = new String[length + 1];
        newVNarray[length] = VN;
        System.arraycopy(VNarray, 0, newVNarray, 0, VNarray.length);
        return newVNarray;
    }


}